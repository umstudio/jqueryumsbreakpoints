(function($){

	var defaults =
	{
		widths              : [],
		syncWidthMediaQuery : false,
		onBreakPoint        : null
	};
	var objects   = [];
	var eventdata = {};
	var options   = {};

	var methods =
	{
		init : function(p_options)
		{
			options = $.extend( {}, defaults, p_options );
			var result = this.each(function()
			{
				$.fn.umsBreakPoints.process($(this), options);
			});
			$.fn.umsBreakPoints.addResizeListener();
			return result;
		}
	};

	$.fn.umsBreakPoints = function(methodOrOptions)
	{
		if ( methods[methodOrOptions] )
		{
			return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		}
		else if ( typeof methodOrOptions === 'object' || ! methodOrOptions )
		{
			return methods.init.apply( this, arguments );
		}
		else
		{
			$.error('Method ' + methodOrOptions + ' does not exist on jQuery.umsBreakPoints');
		}
	};

	$.fn.umsBreakPoints.getWindowWidth = function()
	{
		return (options.syncWidthMediaQuery) ? window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth : $(window).width();
	};

	$.fn.umsBreakPoints.addResizeListener = function()
	{
		eventdata = 
		{
			'curWidth' : $.fn.umsBreakPoints.getWindowWidth(),
			'oldWidth' : 0
		};

		$(window).resize
		(
			function()
			{
				$.fn.umsBreakPoints.processResize();
			}
		);

		$.fn.umsBreakPoints.processResize();
	};

	$.fn.umsBreakPoints.processResize = function()
	{
		var newWidth = $.fn.umsBreakPoints.getWindowWidth();
		var oldWidth = eventdata.curWidth;

		eventdata = 
		{
			'curWidth' : newWidth,
			'oldWidth' : oldWidth
		};

		for(var k in objects)
		{
			var object         = objects[k];
			var options        = object.options;
			var widths         = object.options.widths;
			var curWidth       = 0;
			var new_class_name = '';
			var extremes       = 
			{
				first: widths[widths.length-1],
				last : widths[0]
			};

			if ( eventdata.curWidth <= extremes.first )
			{
				new_class_name = 'lt' + extremes.first.toString();
				eventdata.rule = extremes.first;
			}
			else if ( eventdata.curWidth >= extremes.last )
			{
				new_class_name = 'gt' + extremes.last.toString();
				eventdata.rule = extremes.last;
			}
			else
			{
				for(var i in widths)
				{
					curWidth = widths[i];
					if ( ( eventdata.curWidth >= curWidth ) )
					{
						new_class_name = 'b' + curWidth.toString() + 'a' + widths[i-1];
						eventdata.rule = curWidth;
						break;
					}
				}
			}
			if (new_class_name !== '')
			{
				eventdata.class_name = new_class_name;
				$.fn.umsBreakPoints.applyBreakPoint(object, new_class_name, eventdata);
			}
		}
	};

	$.fn.umsBreakPoints.applyBreakPoint = function(p_object, p_new_class_name, p_event_data)
	{
		if ( (p_object.last_class_name != p_new_class_name) )
		{
			p_object.element.removeClass(p_object.last_class_name).addClass(p_new_class_name);
			p_object.last_class_name = p_new_class_name;
			if (p_object.onBreakPoint !== null)
			{
				var suffix = (p_object.element.attr('id') !== undefined) ? '#' + p_object.element.attr('id') : '';
				p_event_data.element = (p_object.element.prop('tagName') + suffix).toLowerCase();
				p_object.onBreakPoint(p_event_data);
			}
		}
	};

	$.fn.umsBreakPoints.makeId = function()
	{
		var text = "";
		var possible = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz0123456789";
		var pl = possible.length;
		for(var i = 0; i < 8; i++)
		{
			text += possible.charAt(Math.floor(Math.random() * pl));
		}
		return text;
	};

	$.fn.umsBreakPoints.addUniqueId = function(p_elem)
	{
		var ids = $.fn.umsBreakPoints.makeId();
		while (objects[ids] !== undefined)
		{
			ids = $.fn.umsBreakPoints.makeId();
		}
		p_elem.attr('data-breakpoint-ids', ids);
		return ids;
	};

	$.fn.umsBreakPoints.process = function(p_elem, p_options)
	{
		var ids = $.fn.umsBreakPoints.addUniqueId(p_elem);
		var comp = {};
		comp.element         = p_elem;
		comp.options         = p_options;
		comp.onBreakPoint    = p_options.onBreakPoint;
		comp.last_class_name = '';
		comp.options.widths.sort( function(a, b) { return b-a; } );
		objects[ids] = (comp);
	};

})( jQuery );