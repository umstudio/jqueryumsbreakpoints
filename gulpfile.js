const gulp        = require('gulp');
const concat      = require('gulp-concat');
const uglify      = require('gulp-uglify');
const edit        = require('gulp-edit');
const strnow      = require('strnow');
const packageinfo = require('packagejsondata');

function main()
{
	return gulp
		.src
		(
			[
				'src/jqueryUmsBreakPoints.js'
			]
		)
		.pipe(concat('jqueryUmsBreakPoints.js'))
		.pipe
		(
			edit
			(
				(src, cb) =>
				{
					src = '// Version: ' + packageinfo.next() + ' - Last modified: ' + strnow.get() + '\n' + src;
					cb(null, src);
				}
			)
		)
		.pipe(gulp.dest('dist'))
		.pipe(uglify())
		.pipe(concat('jqueryUmsBreakPoints.min.js'))
		.pipe
		(
			edit
			(
				(src, cb) =>
				{
					src = '// Version: ' + packageinfo.next() + ' - Last modified: ' + strnow.get() + '\n' + src;
					cb(null, src);
				}
			)
		)
		.pipe(gulp.dest('dist'))
	;
}

exports.default = main;