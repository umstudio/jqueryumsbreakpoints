$(document).ready(function() {
	
	$('#banner,#desktop-menu,header').umsBreakPoints
	(
		{
			'syncWidthMediaQuery' : true,
			'widths'              : [400, 600, 800],
			'onBreakPoint'        : function(p_eventdata)
			{
				console.log(p_eventdata);
				if ( ($('.menu-active:visible').length > 0) && ($('#desktop-menu:visible').length > 0) )
				{
					$('html').removeClass('menu-active');
				}
			}
		}
	);

	$('.menu-anchor').on('click touchstart', function(e){
		$('html').toggleClass('menu-active');
		e.preventDefault();
	});

});